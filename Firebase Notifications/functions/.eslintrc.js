module.exports = {
  root: true,
  env: {
    es6: true,
    node: true,
  },
  extends: [
    "eslint:recommended",
    "google",
  ],
  rules: {
    "max-len": [2, 200, 4, { "ignoreUrls": true }],
    "object-curly-spacing": ["error", "always"],
    "quotes": ["error", "double"],
  },
};
